/*
 * Copyright (c) 2013, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.chess.pieces;

import java.util.HashSet;
import java.util.Set;
import net.jevring.chess.Movement;
import net.jevring.chess.Player;
import net.jevring.chess.Position;
import net.jevring.chess.board.Board;

/**
 * A bishop that can move diagonally.
 *
 * @author markus@jevring.net
 */
public class Bishop extends Piece {
	public Bishop(Player player, Side side) {
		super(Bishop.class.getSimpleName() + "-" + player + "-" + side,
		      createStartingPosition(player, side),
		      player,
		      Bishop.class);
	}

	private static Position createStartingPosition(Player side, Side corner) {
		int row = side == Player.Black ? 0 : 7;
		int column = corner == Side.Left ? 2 : 5;
		return new Position(row, column);
	}

	@Override
	public Set<Movement> calculateAllowedMovements() {
		Set<Movement> allowedMovements = new HashSet<>();
		movementsFromPosition(allowedMovements, currentPosition, board, player);
		return allowedMovements;
	}

	static void movementsFromPosition(Set<Movement> allowedMovements, Position position, Board board, Player player) {
		// moved this to a static location, as the queen qill use the same logic as the bishop and the rook
		int row;
		int column;

		// up left
		row = position.getRow();
		column = position.getColumn();
		while (row > 0 && column > 0) {
			row--;
			column--;
			if (addAndCheckStop(allowedMovements, new Movement(row, column), board, player)) {
				// the first time we encounter something, we should stop trying, as we can't pass things anyway
				break;
			}
		}

		// down left
		row = position.getRow();
		column = position.getColumn();
		while (row < 7 && column > 0) {
			row++;
			column--;
			if (addAndCheckStop(allowedMovements, new Movement(row, column), board, player)) {
				// the first time we encounter something, we should stop trying, as we can't pass things anyway
				break;
			}
		}

		// up right
		row = position.getRow();
		column = position.getColumn();
		while (row > 0 && column < 7) {
			row--;
			column++;
			if (addAndCheckStop(allowedMovements, new Movement(row, column), board, player)) {
				// the first time we encounter something, we should stop trying, as we can't pass things anyway
				break;
			}
		}

		// down right
		row = position.getRow();
		column = position.getColumn();
		while (row < 7 && column < 7) {
			row++;
			column++;
			if (addAndCheckStop(allowedMovements, new Movement(row, column), board, player)) {
				// the first time we encounter something, we should stop trying, as we can't pass things anyway
				break;
			}
		}
	}
}
