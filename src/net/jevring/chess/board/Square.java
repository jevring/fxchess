/*
 * Copyright (c) 2013, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.chess.board;

import java.util.ArrayList;
import java.util.List;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Paint;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeType;
import net.jevring.chess.Position;
import net.jevring.chess.options.Options;
import net.jevring.chess.pieces.Piece;

/**
 * A piece of the playing board.
 *
 * @author markus@jevring.net
 */
public class Square extends Rectangle {
	private final Position position;
	private boolean validTarget = false;
	private boolean capturing = false;
	private boolean hovering = false;
	private Piece occupant;

	public Square(Color color, Position position) {
		this.position = position;
		setFill(color);
	}

	public Position getPosition() {
		return position;
	}

	public Piece getOccupant() {
		return occupant;
	}

	public void setOccupant(Piece occupant) {
		this.occupant = occupant;
	}

	public void setValidTarget(boolean validTarget) {
		if (this.validTarget != validTarget) {
			this.validTarget = validTarget;
			if ((validTarget && Options.INSTANCE.getHighlightValidTargets()) || !validTarget) {
				// allow unset always, but only set if allowed
				updateStroke();
			}
		}
	}

	public void setHovering(boolean hovering) {
		if (this.hovering != hovering) {
			this.hovering = hovering;
			if ((hovering && Options.INSTANCE.getHighlightHovering()) || !hovering) {
				// allow unset always, but only set if allowed
				updateStroke();
			}
		}
	}

	public void setCapturing(boolean capturing) {
		if (this.capturing != capturing) {
			this.capturing = capturing;
			if ((capturing && Options.INSTANCE.getHighlightCapturing()) || !capturing) {
				// allow unset always, but only set if allowed
				updateStroke();
			}
		}
	}

	public void resetHighlights() {
		this.hovering = false;
		this.validTarget = false;
		this.capturing = false;
		updateStroke();
	}

	private void updateStroke() {
		// as we can't update these values while we're holding a piece anyway, we can just fetch them here.
		// for dynamic properties, we'd have to bind
		boolean showHoveringHighlight = hovering && Options.INSTANCE.getHighlightHovering();
		boolean showValidTargetHighlight = validTarget && Options.INSTANCE.getHighlightValidTargets();
		boolean showCapturingHighlight = capturing && Options.INSTANCE.getHighlightCapturing();
		Paint paint;
		List<Stop> stops = new ArrayList<>();
		if (showHoveringHighlight) {
			stops.add(new Stop(0.25f, Options.INSTANCE.getHoveringColor()));
		}
		if (showValidTargetHighlight) {
			stops.add(new Stop(0.5f, Options.INSTANCE.getValidTargetColor()));
		}
		if (showCapturingHighlight) {
			stops.add(new Stop(0.75f, Options.INSTANCE.getCapturingColor()));
		}
		if (stops.isEmpty()) {
			paint = null;
		} else {
			paint = new LinearGradient(0, 0, 1, 1, true, CycleMethod.NO_CYCLE, stops);
		}
		// changing the stroke changes the SIZE of the square too!
		// not desirable. Ahh, this is only when the stroke type is CENTERED.
		// it would be nice to have it centered, but not if it pushes other things away
		setStroke(paint);
		setStrokeType(StrokeType.INSIDE);
		setStrokeWidth(Options.INSTANCE.getStrokeThickness());
	}
}
