/*
 * Copyright (c) 2013, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.chess.game;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBuilder;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ListViewBuilder;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;

/**
 * This is a persistent gui view of a game. We need this, as opposed to just using the {@link Game} class
 * itself, because we need to initialize and keep the game view even though new games are started.
 *
 * @author markus@jevring.net
 */
public class GameInformationPane extends GridPane {
	
	private final Label currentPlayerLabel;
	private final Label whiteDescription;
	private final Label blackDescription;
	private final ListView<Move> moves;
	private final Button startGameButton;
	private Game game;
	private GameInitializer gameInitializer;

	public GameInformationPane() {
		Label currentPlayerDescription = new Label("Current player: ");
		currentPlayerLabel = new Label("White");
		add(currentPlayerDescription, 0, 0);
		add(currentPlayerLabel, 1, 0);


		Label whiteAlgorithmDescription = new Label("White: ");
		whiteDescription = new Label("");
		add(whiteAlgorithmDescription, 0, 1);
		add(whiteDescription, 1, 1);


		Label blackAlgorithmDescription = new Label("Black: ");
		blackDescription = new Label("");
		add(blackAlgorithmDescription, 0, 2);
		add(blackDescription, 1, 2);

		add(ButtonBuilder.create().onAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent actionEvent) {
				gameInitializer.newGame();
			}
		}).text("New game").build(), 0, 3);
		startGameButton = ButtonBuilder.create().onAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent actionEvent) {
				game.start();
			}
		}).text("Start").tooltip(new Tooltip("Starts the game if white is not human")).build();
		startGameButton.setDisable(true);
		add(startGameButton, 1, 3);
		moves = ListViewBuilder.<Move>create().editable(false).build();
		add(moves, 0, 4, 2, 1);
	}

	public void startNewGame(Game game) {
		this.game = game;
		currentPlayerLabel.textProperty().bind(game.currentPlayerProperty());
		whiteDescription.setText(game.getWhitePlayerDescription());
		blackDescription.setText(game.getBlackPlayerDescription());
		moves.setItems(game.getMoves());
		startGameButton.setDisable(game.whiteIsHuman());
	}

	public void setGameInitializer(GameInitializer gameInitializer) {
		this.gameInitializer = gameInitializer;
	}
}
