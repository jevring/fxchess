/*
 * Copyright (c) 2013, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.chess.options;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.paint.Color;

/**
 * This class contains {@link javafx.beans.value.ObservableValue observable values} for all options in the application.
 * The options gui will bind against these when presenting options, and the parts that use the options will also bind
 * against the same.
 *
 * @author markus@jevring.net
 */
public class Options {
	private final BooleanProperty highlightValidTargets = new SimpleBooleanProperty(true);
	private final BooleanProperty highlightHovering = new SimpleBooleanProperty(true);
	private final BooleanProperty highlightCapturing = new SimpleBooleanProperty(true);
	private final DoubleProperty strokeThickness = new SimpleDoubleProperty(10);
	
	private final ObjectProperty<Color> hoveringColor = new SimpleObjectProperty<>();
	private final ObjectProperty<Color> validTargetColor = new SimpleObjectProperty<>();
	private final ObjectProperty<Color> capturingColor = new SimpleObjectProperty<>();
	
	public static final Options INSTANCE = new Options();
	
	private Options() {
		hoveringColor.setValue(Color.ORANGE);
		validTargetColor.setValue(Color.GREEN);
		capturingColor.setValue(Color.BLUE);
	}
	
	// mass getters and setters

	public boolean getHighlightValidTargets() {
		return highlightValidTargets.get();
	}

	public BooleanProperty highlightValidTargetsProperty() {
		return highlightValidTargets;
	}

	public boolean getHighlightHovering() {
		return highlightHovering.get();
	}

	public BooleanProperty highlightHoveringProperty() {
		return highlightHovering;
	}

	public boolean getHighlightCapturing() {
		return highlightCapturing.get();
	}

	public BooleanProperty highlightCapturingProperty() {
		return highlightCapturing;
	}

	public double getStrokeThickness() {
		return strokeThickness.get();
	}

	public DoubleProperty strokeThicknessProperty() {
		return strokeThickness;
	}

	public Color getHoveringColor() {
		return hoveringColor.get();
	}

	public ObjectProperty<Color> hoveringColorProperty() {
		return hoveringColor;
	}

	public Color getValidTargetColor() {
		return validTargetColor.get();
	}

	public ObjectProperty<Color> validTargetsColorProperty() {
		return validTargetColor;
	}

	public Color getCapturingColor() {
		return capturingColor.get();
	}

	public ObjectProperty<Color> capturingColorProperty() {
		return capturingColor;
	}
}
