/*
 * Copyright (c) 2013, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.chess.game;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.util.StringConverter;
import net.jevring.chess.Player;
import net.jevring.chess.images.Images;
import net.jevring.chess.pieces.King;

/**
 * Controller for the "new game" dialog. Bound against FXML.
 *
 * @author markus@jevring.net
 */
public class NewGameDialogController implements Initializable {
	// interestingly, as long as the fx:id matches the name here, we don't need the FXML annotation.
	// It's ugly that these things need to be public, but it's understandable given the dynamic initialization
	public ChoiceBox<Algorithm> whiteComputerSelection;
	public ChoiceBox<Algorithm> blackComputerSelection;
	public ToggleGroup black;
	public ToggleGroup white;
	public ImageView image;
	public RadioButton whiteHuman;
	public RadioButton blackHuman;
	public AnchorPane root;
	private GameInitializer gameInitializer;

	public void disableWhiteAlgorithmSelection(ActionEvent actionEvent) {
		whiteComputerSelection.setDisable(true);
	}

	public void disableBlackAlgorithmSelection(ActionEvent actionEvent) {
		blackComputerSelection.setDisable(true);
	}

	public void enableWhiteAlgorithmSelection(ActionEvent actionEvent) {
		whiteComputerSelection.setDisable(false);
		whiteComputerSelection.setValue(whiteComputerSelection.getItems().get(0));
	}

	public void enableBlackAlgorithmSelection(ActionEvent actionEvent) {
		blackComputerSelection.setDisable(false);
		blackComputerSelection.setValue(blackComputerSelection.getItems().get(0));
	}

	public void createNewGame(ActionEvent actionEvent) {
		Algorithm whiteAlgorithm;
		Algorithm blackAlgorithm;
		if (white.getSelectedToggle() == whiteHuman) {
			whiteAlgorithm = Human.INSTANCE;
		} else {
			whiteAlgorithm = whiteComputerSelection.getValue();
		}
		if (black.getSelectedToggle() == blackHuman) {
			blackAlgorithm = Human.INSTANCE;
		} else {
			blackAlgorithm = blackComputerSelection.getValue();
		}
		gameInitializer.createNewGame(whiteAlgorithm, blackAlgorithm);
	}

	public void cancelDialog(ActionEvent actionEvent) {
		gameInitializer.cancelDialog();
	}

	@Override
	public void initialize(URL url, ResourceBundle resourceBundle) {
		image.setImage(Images.getImageSet().getImage(King.class, Player.White));
		/* Apparently it doesn't work to set the size here. Why?
		image.setPreserveRatio(true);
		image.setFitHeight(image.getFitHeight() * 2d);
		*/

		// todo: get the fucking sources working again so we can see what's going on!
		final ObservableList<Algorithm> algorithms =
				FXCollections.observableArrayList(new CapturingMovementFirst(), new RandomAllowedMovement());
		whiteComputerSelection.setItems(algorithms);
		blackComputerSelection.setItems(algorithms);
		
		StringConverter<Algorithm> algorithmNameConverter = new StringConverter<Algorithm>() {
			@Override
			public String toString(Algorithm algorithm) {
				return algorithm.getName();
			}

			@Override
			public Algorithm fromString(String s) {
				for (Algorithm algorithm : algorithms) {
					if (algorithm.getName().equals(s)) {
						return algorithm;
					}
				}
				return null;
			}
		};
		whiteComputerSelection.setConverter(algorithmNameConverter);
		blackComputerSelection.setConverter(algorithmNameConverter);
	}

	public void setGameInitializer(GameInitializer gameInitializer) {
		this.gameInitializer = gameInitializer;
	}
}
