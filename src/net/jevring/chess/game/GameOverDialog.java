/*
 * Copyright (c) 2013, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.chess.game;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBuilder;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.HBoxBuilder;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.layout.VBoxBuilder;
import net.jevring.chess.DialogCreator;
import net.jevring.chess.Player;

/**
 * Used to show that the game is over, and provides the ability to start a new one.
 *
 * @author markus@jevring.net
 */
public class GameOverDialog extends BorderPane {
	public GameOverDialog(Player winner, final GameInitializer gameInitializer, final DialogCreator dialogCreator) {
		
		Label label = new Label("Player " + winner + " has won. Reset game?");
		label.setAlignment(Pos.BASELINE_CENTER);
		Button yes = ButtonBuilder.create().text("Yes").prefHeight(35).prefWidth(75).onAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent actionEvent) {
				gameInitializer.newGame();
			}
		}).build();
		Button no = ButtonBuilder.create().text("No").prefHeight(35).prefWidth(75).onAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent actionEvent) {
				dialogCreator.hideDialog();
			}
		}).build();

		HBox buttons =
				HBoxBuilder.create().spacing(15).padding(new Insets(0, 20, 10, 20)).alignment(Pos.BASELINE_CENTER).children(yes,
				                                                                                                            no).build();

		VBox root = VBoxBuilder.create().spacing(15).padding(new Insets(20, 20, 20, 20)).alignment(Pos.TOP_CENTER).children(label,
		                                                                                                    buttons).build();
		setMaxSize(300, 110);
		setCenter(root);
	}
}
