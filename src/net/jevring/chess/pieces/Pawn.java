/*
 * Copyright (c) 2013, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.chess.pieces;

import java.util.HashSet;
import java.util.Set;
import net.jevring.chess.Movement;
import net.jevring.chess.Player;
import net.jevring.chess.Position;

/**
 * Encapsulates the logic of a pawn.
 *
 * @author markus@jevring.net
 */
public class Pawn extends Piece {
	/**
	 * @param n Which pawn is this. From left to right.
	 */
	public Pawn(Player side, int n) {
		super(Pawn.class.getSimpleName() + "-" + side + "-" + n, calculateStartingPosition(side, n), side, Pawn.class);
		if (n < 0 || n > 7) {
			throw new IllegalArgumentException("'n' must be between 0 and 7, inclusive");
		}
	}

	private static Position calculateStartingPosition(Player side, int n) {
		if (side == Player.Black) {
			return new Position(1, n);
		} else {
			return new Position(6, n);
		}
	}

	@Override
	public Set<Movement> calculateAllowedMovements() {
		// if we are in the start position, we allow 2 steps forward, otherwise only one.
		// todo: implement esoteric taking-while-passing bullshit later
		// https://en.wikipedia.org/wiki/En_passant
		Set<Movement> allowedMovements = new HashSet<>();
		Movement baseMovement = new Movement(currentPosition.getRow(), currentPosition.getColumn());
		if (player == Player.Black) {
			if (currentPosition.getRow() != 7) {
				addIfNotOccupied(allowedMovements, baseMovement.moveByRows(1));
				if (atStartPosition()) {
					addIfNotOccupied(allowedMovements, baseMovement.moveByRows(2));
				}
			}
		} else {
			if (currentPosition.getRow() != 0) {
				addIfNotOccupied(allowedMovements, baseMovement.moveByRows(-1));
				if (atStartPosition()) {
					addIfNotOccupied(allowedMovements, baseMovement.moveByRows(-2));
				}
			}
		}
		addPotentialCapturingMove(allowedMovements, baseMovement, -1);
		addPotentialCapturingMove(allowedMovements, baseMovement, 1);

		return allowedMovements;
	}

	private void addIfNotOccupied(Set<Movement> allowedMovements, Movement movement) {
		if (!board.isOccupied(movement)) {
			allowedMovements.add(movement);
		}
	}

	private void addPotentialCapturingMove(Set<Movement> allowedMovements, Movement baseMovement, int direction) {
		Movement movement;
		if (player == Player.Black) {
			movement = baseMovement.moveBy(1, direction);
		} else {
			movement = baseMovement.moveBy(-1, direction);
		}
		if (movement.isInsideBoard()) {
			Piece occupant = board.getOccupant(movement);
			if (occupant != null && occupant.getPlayer() != player) {
				allowedMovements.add(movement.toCapturing(occupant));
			}
		}
	}

	private boolean atStartPosition() {
		return currentPosition.equals(getStartingPosition());
	}
}
