/*
 * Copyright (c) 2013, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.chess.pieces;

import java.util.HashSet;
import java.util.Set;
import net.jevring.chess.Movement;
import net.jevring.chess.Player;
import net.jevring.chess.Position;

/**
 * The knight is the only character who can "jump" over other characters. It has a 2,1 movement
 * in any direction, effectively resulting in 8 potential moves.
 *
 * @author markus@jevring.net
 */
public class Knight extends Piece {
	public Knight(Player player, Side side) {
		super(Knight.class.getSimpleName() + "-" + player + "-" + side,
		      createStartingPosition(player, side),
		      player,
		      Knight.class);
	}

	private static Position createStartingPosition(Player side, Side corner) {
		int row = side == Player.Black ? 0 : 7;
		int column = corner == Side.Left ? 1 : 6;
		return new Position(row, column);
	}

	@Override
	public Set<Movement> calculateAllowedMovements() {
		Set<Movement> allowedMovements = new HashSet<>();

		// down left
		addIfAllowed(currentPosition.moveBy(2, -1), allowedMovements);
		// down right
		addIfAllowed(currentPosition.moveBy(2, 1), allowedMovements);

		// up left
		addIfAllowed(currentPosition.moveBy(-2, -1), allowedMovements);
		// up right
		addIfAllowed(currentPosition.moveBy(-2, 1), allowedMovements);

		// left up
		addIfAllowed(currentPosition.moveBy(-1, -2), allowedMovements);
		// left down
		addIfAllowed(currentPosition.moveBy(-1, 2), allowedMovements);

		// right up
		addIfAllowed(currentPosition.moveBy(1, -2), allowedMovements);
		// right down
		addIfAllowed(currentPosition.moveBy(1, 2), allowedMovements);


		return allowedMovements;
	}

	private void addIfAllowed(Position position, Set<Movement> allowedMovements) {
		if (position.isInsideBoard()) {
			addAndCheckStop(allowedMovements, new Movement(position), board, player);
		}
	}
}
