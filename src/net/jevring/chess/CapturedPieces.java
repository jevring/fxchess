/*
 * Copyright (c) 2013, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.chess;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import net.jevring.chess.images.Images;
import net.jevring.chess.pieces.Bishop;
import net.jevring.chess.pieces.King;
import net.jevring.chess.pieces.Knight;
import net.jevring.chess.pieces.Pawn;
import net.jevring.chess.pieces.Piece;
import net.jevring.chess.pieces.Queen;
import net.jevring.chess.pieces.Rook;

/**
 * A view on all the currently captured pieces.
 *
 * @author markus@jevring.net
 */
public class CapturedPieces extends GridPane {
	private final Map<CaptureViewKey, CaptureView> captureViews = new HashMap<>();
	private final Set<Piece> black = new HashSet<>();
	private final Set<Piece> white = new HashSet<>();
	/*
	We should have a column for white, and one for black.
	There should be placeholders that are grey for each type.
	For pieces that there can be more of (almost all of them), we should have a little superscript counter, like 1/2 or 6/8
	 */

	public CapturedPieces() {
		setPadding(new Insets(2));

		setUp();
	}
	
	public void capture(Piece piece) {
		Class<? extends Piece> pieceClass = piece.getClass();
		Player player = piece.getPlayer();

		if (player == Player.Black) {
			black.add(piece);
		} else {
			white.add(piece);
		}
		getCaptureView(pieceClass, player).setCaptures(captures(pieceClass, player));
	}

	public void reset() {
		black.clear();
		white.clear();
		for (CaptureView captureView : captureViews.values()) {
			captureView.setCaptures(0);
		}
	}

	private CaptureView getCaptureView(Class<? extends Piece> pieceClass, Player player) {
		return captureViews.get(new CaptureViewKey(pieceClass, player));
	}

	private void setUp() {
		// kings
		// kings don't spend any time being captured, so lets not include them in the list
		//addCapturedPiece(King.class, Player.White, 0);
		//addCapturedPiece(King.class, Player.Black, 0);
		
		Label label = new Label("Captured pieces");
		label.setStyle("-fx-label-padding: 4;");
		add(label, 0, 0, 2, 1);

		// kings (only one of these will ever be captured, but it's handy to have it here anyway, for completeness)
		addCapturedPiece(King.class, Player.White, 2);
		addCapturedPiece(King.class, Player.Black, 2);
		
		// queens
		addCapturedPiece(Queen.class, Player.White, 3);
		addCapturedPiece(Queen.class, Player.Black, 3);
		
		// bishops (2)
		addCapturedPiece(Bishop.class, Player.White, 4);
		addCapturedPiece(Bishop.class, Player.Black, 4);
		
		// knights (2)
		addCapturedPiece(Knight.class, Player.White, 5);
		addCapturedPiece(Knight.class, Player.Black, 5);
		
		// rooks (2)
		addCapturedPiece(Rook.class, Player.White, 6);
		addCapturedPiece(Rook.class, Player.Black, 6);
		
		// pawns (8)
		addCapturedPiece(Pawn.class, Player.White, 7);
		addCapturedPiece(Pawn.class, Player.Black, 7);
	}
	
	private void addCapturedPiece(Class<? extends Piece> pieceClass, Player player, int row) {
		CaptureView captureView = new CaptureView(pieceClass, player);
		CaptureViewKey key = new CaptureViewKey(pieceClass, player);
		captureViews.put(key, captureView);
		
		
		add(captureView, getColumn(player), row);
	}
	
	private int getColumn(Player player) {
		if (player == Player.Black) {
			return 1;
		} else {
			return 0;
		}
	}

	private int captures(Class<? extends Piece> pieceClass, Player player) {
		int captures = 0;
		for (Piece capturedPiece : getCapturedPieces(player)) {
			if (capturedPiece.getClass().equals(pieceClass)) {
				captures++;
			}
		}
		return captures;
	}

	public Collection<Piece> getCapturedPieces(Player player) {
		if (player == Player.Black) {
			return Collections.unmodifiableCollection(black);
		} else {
			return Collections.unmodifiableCollection(white);
		}
	}

	public Collection<Piece> getCapturedPieces() {
		Set<Piece> pieces = new HashSet<>(black);
		pieces.addAll(white);
		return Collections.unmodifiableCollection(pieces);
	} 
	
	private static final class CaptureView extends StackPane {
		private static final double UNCAPTURED_OPACITY = 0.3;
		private final ImageView imageView;
		private final Label label;
		private final int maxCaptures;

		private CaptureView(Class<? extends Piece> pieceClass, Player player) {
			imageView = new ImageView(Images.getImageSet().getImage(pieceClass, player));
			// start like this, as we start with 0 captures
			imageView.setOpacity(UNCAPTURED_OPACITY);

			getChildren().addAll(imageView);
			maxCaptures = maxCaptures(pieceClass);
			if (maxCaptures > 1) {
				label = new Label("0 / " + maxCaptures);
				AnchorPane anchorPane = new AnchorPane();
				AnchorPane.setTopAnchor(label, 0d);
				AnchorPane.setRightAnchor(label, 0d);
				anchorPane.getChildren().add(label);
				getChildren().add(anchorPane);
			} else {
				label = null;
			}
			// http://docs.oracle.com/javafx/2/api/javafx/scene/doc-files/cssref.html
			setStyle("-fx-border-color: lightgrey; -fx-border-width: 2; -fx-border-insets: 4;");
			setPrefSize(80, 80);
		}

		private int maxCaptures(Class<? extends Piece> pieceClass) {
			if (pieceClass.equals(Pawn.class)) {
				return 8;
			} else if (pieceClass.equals(Queen.class) || pieceClass.equals(King.class)) {
				return 1;
			} else {
				return 2;
			}
		}
		
		private void setCaptures(int i) {
			if (i > 0) {
				imageView.setOpacity(1);
			} else {
				imageView.setOpacity(UNCAPTURED_OPACITY);
			}
			if (label != null) {
				label.setText(i + " / " + maxCaptures);
			}
		}
	}
	
	private static final class CaptureViewKey {
		private final Class<? extends Piece> clazz;
		private final Player player;

		private CaptureViewKey(Class<? extends Piece> clazz, Player player) {
			this.clazz = clazz;
			this.player = player;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) {
				return true;
			}
			if (o == null || getClass() != o.getClass()) {
				return false;
			}

			CaptureViewKey that = (CaptureViewKey) o;

			if (!clazz.equals(that.clazz)) {
				return false;
			}
			if (player != that.player) {
				return false;
			}

			return true;
		}

		@Override
		public int hashCode() {
			int result = clazz.hashCode();
			result = 31 * result + player.hashCode();
			return result;
		}
	}
}
