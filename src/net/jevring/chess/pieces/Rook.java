/*
 * Copyright (c) 2013, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.chess.pieces;

import java.util.HashSet;
import java.util.Set;
import net.jevring.chess.Movement;
import net.jevring.chess.Player;
import net.jevring.chess.Position;
import net.jevring.chess.board.Board;

/**
 * A rook can move in straight lines.
 *
 * @author markus@jevring.net
 */
public class Rook extends Piece {
	public Rook(Player player, Side side) {
		super(Rook.class.getSimpleName() + "-" + player + "-" + side, createStartingPosition(player, side), player, Rook.class);
	}

	private static Position createStartingPosition(Player side, Side corner) {
		int row = side == Player.Black ? 0 : 7;
		int column = corner == Side.Left ? 0 : 7;
		return new Position(row, column);
	}

	@Override
	public Set<Movement> calculateAllowedMovements() {
		Set<Movement> allowedMovements = new HashSet<>();
		movementsFromPosition(allowedMovements, currentPosition, board, player);
		return allowedMovements;
	}

	static void movementsFromPosition(Set<Movement> allowedMovements, Position position, Board board, Player player) {
		// left
		for (int column = position.getColumn() - 1; column >= 0; column--) {
			Position p = position.moveToColumn(column);
			if (addAndCheckStop(allowedMovements, new Movement(p), board, player)) {
				// the first time we encounter something, we should stop trying, as we can't pass things anyway
				break;
			}
		}

		// right
		for (int column = position.getColumn() + 1; column <= 7; column++) {
			Position p = position.moveToColumn(column);
			if (addAndCheckStop(allowedMovements, new Movement(p), board, player)) {
				// the first time we encounter something, we should stop trying, as we can't pass things anyway
				break;
			}
		}

		// up
		for (int row = position.getRow() - 1; row >= 0; row--) {
			Position p = position.moveToRow(row);
			if (addAndCheckStop(allowedMovements, new Movement(p), board, player)) {
				// the first time we encounter something, we should stop trying, as we can't pass things anyway
				break;
			}
		}

		// down
		for (int row = position.getRow() + 1; row <= 7; row++) {
			Position p = position.moveToRow(row);
			if (addAndCheckStop(allowedMovements, new Movement(p), board, player)) {
				// the first time we encounter something, we should stop trying, as we can't pass things anyway
				break;
			}
		}
	}
}
