/*
 * Copyright (c) 2013, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.chess.board;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import net.jevring.chess.CapturedPieces;
import net.jevring.chess.Movement;
import net.jevring.chess.Player;
import net.jevring.chess.Position;
import net.jevring.chess.game.Game;
import net.jevring.chess.game.Move;
import net.jevring.chess.pieces.Bishop;
import net.jevring.chess.pieces.King;
import net.jevring.chess.pieces.Knight;
import net.jevring.chess.pieces.Pawn;
import net.jevring.chess.pieces.Piece;
import net.jevring.chess.pieces.Queen;
import net.jevring.chess.pieces.Rook;
import net.jevring.chess.pieces.Side;

/**
 * THe board on which the pieces move, and the game takes place.
 *
 * @author markus@jevring.net
 */
public class Board extends StackPane {
	// todo: at the top or bottom, we should have an into thing. Saying "White, make a move", and "Algorithm X is thinking about a move for Black"
	
	// todo: a piece in "check" should be highlighted
	
	// todo: for each move, we should check "check-mate". If the opponent is in "check mate", you win directly
	
	// todo: detect draws: https://en.wikipedia.org/wiki/Draw_%28chess%29
	
	// todo: enable coordinate systems. rows 1 - 8, starting at the bottom. Columns a - h, starting from the left
	
	
	private final Square[][] squares = new Square[8][8];
	private final Map<String, Piece> pieces = new HashMap<>();
	private final GridPane board = new GridPane();
	private final CapturedPieces capturedPieces;
	private Game game;

	public Board(CapturedPieces capturedPieces) {
		this.capturedPieces = capturedPieces;
		
		// todo: use ids and style the squares instead?
		Color color = Color.WHITE;
		for (int row = 0; row < 8; row++) {
			// we need to switch on BOTH row AND column, otherwise we end up with a zebra pattern
			if (color == Color.DARKGRAY) {
				color = Color.WHITE;
			} else {
				color = Color.DARKGRAY;
			}
			for (int column = 0; column < 8; column++) {
				if (color == Color.DARKGRAY) {
					color = Color.WHITE;
				} else {
					color = Color.DARKGRAY;
				}
				Square square = createSquare(color, row, column);

				// Putting this in a BorderPane does nothing. It still doesn't scale
				board.add(square, column, row);
				squares[row][column] = square;
			}
		}

		// setting it on the board means that we don't move the icon until we move
		// the cursor outside the piece. Perhaps this is ok.
		// if not, we'll have to do this for the piece as well.
		// NOTE: this assumes that the piece was clicked in the middle. This is likely acceptable, at least for now
		board.setOnDragOver(new EventHandler<DragEvent>() {
			@Override
			public void handle(DragEvent dragEvent) {
				//System.out.println("Drag over: " + dragEvent);
				showFloatingPiece(dragEvent, board);
				dragEvent.consume();
			}
		});

		// this is needed to handle the case where a drop is on an illegal square.
		// in this case we must still hide the drag icon
		board.setOnDragDone(new EventHandler<DragEvent>() {
			@Override
			public void handle(DragEvent dragEvent) {
				Piece piece = getPiece(dragEvent);
				if (piece == null) {
					// this happens when we drag OTHER things over this, like files in windows
					return;
				}
				//System.out.println("Drag done: " + dragEvent);
				piece.getDragImage().setVisible(false);
				resetSquareHighlights();
				dragEvent.consume();
			}
		});
		
		getChildren().add(board);
	}

	public boolean isOccupied(Position position) {
		return getOccupant(position) != null;
	}

	public Piece getOccupant(Position position) {
		return getPiece(getSquare(position));
	}

	private Square getSquare(Position position) {
		return squares[position.getRow()][position.getColumn()];
	}

	private void add(Piece piece) {
		decoratePiece(piece);
		pieces.put(piece.getId(), piece);
		piece.addedToBoard(this);
	}
	
	private void remove(Piece piece) {
		pieces.remove(piece.getId());
		getChildren().remove(piece);
	}

	private void capture(Piece attacker, Piece target) {
		// todo: slide the captured piece up to the capture board

		remove(target);
		capturedPieces.capture(target);
		move(attacker, target.getCurrentPosition());
	}

	/**
	 * Moves the pieces to their starting positions. This has to be done AFTER the parent has been
	 * made visible, as otherwise the squares won't have either size or location.
	 */
	public void newGame(Game game) {
		this.game = game;
		for (Piece piece : pieces.values()) {
			getChildren().removeAll(piece, piece.getDragImage());
		}
		pieces.clear();
		
		// remove occupants from every square
		for (Square[] row : squares) {
			for (Square square : row) {
				square.setOccupant(null);
				square.resetHighlights();
			}
		}
		
		capturedPieces.reset();
				
		
		// pawns
		for (int i = 0; i < 8; i++) {
			add(new Pawn(Player.White, i));
			add(new Pawn(Player.Black, i));
		}

		// rooks
		add(new Rook(Player.White, Side.Left));
		add(new Rook(Player.White, Side.Right));
		add(new Rook(Player.Black, Side.Left));
		add(new Rook(Player.Black, Side.Right));

		// bishops
		add(new Bishop(Player.White, Side.Left));
		add(new Bishop(Player.White, Side.Right));
		add(new Bishop(Player.Black, Side.Left));
		add(new Bishop(Player.Black, Side.Right));

		// knights
		add(new Knight(Player.White, Side.Left));
		add(new Knight(Player.White, Side.Right));
		add(new Knight(Player.Black, Side.Left));
		add(new Knight(Player.Black, Side.Right));

		// kings
		add(new King(Player.White));
		add(new King(Player.Black));

		// queens
		add(new Queen(Player.White));
		add(new Queen(Player.Black));
		
		// we have to do this here, otherwise the rectangles don't have positions yet
		for (Piece p : pieces.values()) {
			getChildren().add(p);
			movePiece(p, getSquare(p.getStartingPosition()));
		}
		updateAllowedMovementsForAllPieces();
		resetSquareHighlights();
	}

	/**
	 * Decorate the piece with drag-and-drop semantics.
	 * 
	 * @param piece the piece to be decorated
	 */
	private void decoratePiece(final Piece piece) {
		piece.setOnDragDetected(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent mouseEvent) {
				if (!game.isPlayersTurn(piece.getPlayer())) {
					// it's not our turn!
					return;
				}
				//System.out.println("Drag detected: " + mouseEvent);
				ImageView dragImage = piece.getDragImage();
				if (!getChildren().contains(dragImage)) {
					getChildren().add(dragImage);
				}

				dragImage.toFront();
				dragImage.setVisible(true);
				// we get the drag-and-drop cursor instead, which is fine. 
				// also, this doesn't work anyway =(
				//dragImage.setCursor(Cursor.DISAPPEAR);

				dragImage.relocate(piece.getLayoutX(), piece.getLayoutY());

				// start the drag'n'drop sequence
				Dragboard dragboard = board.startDragAndDrop(TransferMode.MOVE);

				// this is just a glorified map
				ClipboardContent clipboardContent = new ClipboardContent();
				// without content, the drag sequence never happens. We never get the OnDragOver event
				clipboardContent.putString(piece.getId());

				dragboard.setContent(clipboardContent);
				mouseEvent.consume();

				for (Movement movement : piece.getAllowedMovements()) {
					Square square = getSquare(movement);
					square.setValidTarget(true);
					square.setCapturing(movement.isCapturing());
				}
			}
		});

		piece.setOnDragOver(new EventHandler<DragEvent>() {
			@Override
			public void handle(DragEvent dragEvent) {
				showFloatingPiece(dragEvent, piece);
				Piece attacker = getPiece(dragEvent);
				if (attacker == null) {
					// this happens when we drag OTHER things over this, like files in windows
					return;
				}

				Square square = getSquare(piece.getCurrentPosition());
				square.setHovering(true);
				// Position.equals() handles both Position and Movement
				//noinspection SuspiciousMethodCalls
				if (attacker.getAllowedMovements().contains(piece.getCurrentPosition())) {
					dragEvent.acceptTransferModes(TransferMode.ANY);
					square.setCapturing(true);
				}
			}
		});

		piece.setOnDragDropped(new EventHandler<DragEvent>() {
			@Override
			public void handle(DragEvent dragEvent) {
				Piece attacker = getPiece(dragEvent);
				if (attacker == null) {
					// this happens when we drag OTHER things over this, like files in windows
					return;
				}
				// We can call capture right away, as we wouldn't even be able to enter here
				// if the OnDragOver handler hadn't tagged this as an acceptable drop point
				// using DragEvent.acceptTransferModes().
				// If we really wanted to, we could check if the square has capturing turned on, 
				// but we don't need to go that far.
				capture(attacker, piece);
				dragEvent.setDropCompleted(true);
				//System.out.println("Piece " + attacker + " captured " + piece);
			}
		});
		piece.setOnMouseEntered(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent mouseEvent) {
				getSquare(piece.getCurrentPosition()).setHovering(true);
			}
		});
		piece.setOnMouseExited(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent mouseEvent) {
				getSquare(piece.getCurrentPosition()).setHovering(false);
			}
		});
	}

	private Square createSquare(Color color, final int row, final int column) {
		// what if I just want something that scales with the size of the container it is in?
		final Square square = new Square(color, new Position(row, column));

		// ideally we'd like this to grow to whatever the parent allows it to, but that seems impossible
		square.setWidth(60);
		square.setHeight(60);

		square.setOnMouseEntered(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent mouseEvent) {
				square.setHovering(true);
			}
		});
		square.setOnMouseExited(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent mouseEvent) {
				square.setHovering(false);
			}
		});

	    /*
	    StrokeTransitionBuilder transitionBuilder = StrokeTransitionBuilder.create();
	    transitionBuilder.shape(square);
	    transitionBuilder.duration(Duration.seconds(10));
	    transitionBuilder.fromValue(Color.BLUE);
	    transitionBuilder.toValue(Color.RED);
	    StrokeTransition strokeTransition = transitionBuilder.build();
	    strokeTransition.playFromStart();
	    */

		// http://docs.oracle.com/javafx/2/drag_drop/jfxpub-drag_drop.htm

		

		square.setOnDragOver(new EventHandler<DragEvent>() {
			@Override
			public void handle(DragEvent dragEvent) {
				Piece piece = getPiece(dragEvent);
				if (piece == null) {
					// this happens when we drag OTHER things over this, like files in windows
					return; 
				}
				// we can suppress this warning, as we know that Position.equals() supports Movement as well
				//noinspection SuspiciousMethodCalls
				if (piece.getAllowedMovements().contains(square.getPosition())) {
					dragEvent.acceptTransferModes(TransferMode.MOVE);
					square.setValidTarget(true);
				} else {
					square.setValidTarget(false);
				}
				square.setHovering(true);
			}
		});
		square.setOnDragExited(new EventHandler<DragEvent>() {
			@Override
			public void handle(DragEvent dragEvent) {
				square.setHovering(false);
				dragEvent.consume();
			}
		});
		square.setOnDragDropped(new EventHandler<DragEvent>() {
			@Override
			public void handle(DragEvent dragEvent) {
				// _todo: how come we don't end up here when there is an invalid movement?
				// likely because we don't set the allowed transfer moves.
				// that way, if the mouse button is released, it's just a dropStopped or something
				
				// if there's another piece here, capture it
				Piece attacker = getPiece(dragEvent);
				if (attacker == null) {
					// this happens when we drag OTHER things over this, like files in windows
					return;
				}
				Piece target = getPiece(square);
				if (target != null) {
					capture(attacker, target);
				} else {
					//System.out.println("Drag done: " + dragEvent);
					move(attacker, square);
				}
				
				dragEvent.setDropCompleted(true);
			}
		});
		return square;
	}

	private Piece getPiece(Square square) {
		return square.getOccupant();
	}

	private Piece getPiece(DragEvent dragEvent) {
		Dragboard dragboard = dragEvent.getDragboard();
		return pieces.get(dragboard.getString());
	}

	private void showFloatingPiece(DragEvent dragEvent, Node source) {
		Piece piece = getPiece(dragEvent);
		if (piece == null) {
			// this happens when we drag OTHER things over this, like files in windows
			return;
		}

		// if we don't get this transform here, we can over over the board but not over other pieces
		double xTransform = source.getLocalToParentTransform().getTx();
		double yTransform = source.getLocalToParentTransform().getTy();
		double xTarget = dragEvent.getX() + xTransform;
		double yTarget = dragEvent.getY() + yTransform;

		double xPieceOffset = (piece.getDragImage().getImage().getWidth() / 2d);
		double yPieceOffset = (piece.getDragImage().getImage().getHeight() / 2d);

		double x = xTarget - xPieceOffset;
		double y = yTarget - yPieceOffset;
		piece.getDragImage().relocate(x, y);
	}

	private void move(Piece piece, Position position) {
		move(piece, getSquare(position));
	}

	private void move(Piece piece, Square target) {
		Movement whichMovementGotUsHere = piece.getMovementForTarget(target.getPosition());
		movePiece(piece, target);

		piece.getDragImage().setVisible(false);
		
		resetSquareHighlights();
		// we have to do this, because the pieces take the board into account
		updateAllowedMovementsForAllPieces();
		game.pieceMoved(new Move(piece, whichMovementGotUsHere));
	}

	private void movePiece(Piece piece, Square square) {
		double xMidTarget = square.getLayoutX() + (square.getWidth() / 2d);
		double yMidTarget = square.getLayoutY() + (square.getHeight() / 2d);

		double xPieceOffset = (piece.getImage().getWidth() / 2d);
		double yPieceOffset = (piece.getImage().getHeight() / 2d);

		double x = xMidTarget - xPieceOffset;
		double y = yMidTarget - yPieceOffset;
		piece.relocate(x, y);
		getSquare(piece.getCurrentPosition()).setOccupant(null); // it's no longer the occupant of the old location
		piece.pieceMoved(square.getPosition());
		square.setOccupant(piece);
	}

	private void updateAllowedMovementsForAllPieces() {
		for (Piece piece : pieces.values()) {
			piece.updateAllowedMovements();
		}
	}

	private void resetSquareHighlights() {
		for (Square[] row : squares) {
			for (Square square : row) {
				square.resetHighlights();
			}
		}
	}
	
	// todo: have arrows that move us back and forward in the game's history.

	public void move(Move move) {
		if (move.getPiece() == getPiece(getSquare(move.getPiece().getCurrentPosition()))) {
			if (move.getPiece().getAllowedMovements().contains(move.getMovement())) {
				// because we've validated that the piece belongs to the board, 
				// and that the movements come from its list of allowed movements, 
				// we can rely on the isCapturing() value
				if (move.getMovement().isCapturing()) {
					capture(move.getPiece(), getOccupant(move.getMovement()));
				} else {
					move(move.getPiece(), move.getMovement());
				}
			} else {
				throw new IllegalArgumentException("Cannot perform movement that are not part of the piece's allowed movements: " + move);
			}
		} else {
			throw new IllegalArgumentException("Cannot move pieces that are not part of the board: " + move);
		}
	}

	public List<Piece> getPieces(Player player) {
		List<Piece> pieces = new ArrayList<>();
		for (Piece piece : this.pieces.values()) {
			if (piece.getPlayer() == player) {
				pieces.add(piece);
			}
		}
		return pieces;
	}
}
