/*
 * Copyright (c) 2013, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.chess.pieces;

import java.util.HashSet;
import java.util.Set;
import net.jevring.chess.Movement;
import net.jevring.chess.Player;
import net.jevring.chess.Position;

/**
 * A king is the objective of the game, and can move a single step in all directions.
 *
 * @author markus@jevring.net
 */
public class King extends Piece {
	public King(Player player) {
		super(King.class.getSimpleName() + "-" + player, createStartingPosition(player), player, King.class);
	}

	private static Position createStartingPosition(Player side) {
		int row = side == Player.Black ? 0 : 7;
		return new Position(row, 4);
	}

	@Override
	public Set<Movement> calculateAllowedMovements() {
		Set<Movement> allowedMovements = new HashSet<>();

		addIfAllowed(currentPosition.moveBy(1, 0), allowedMovements);
		addIfAllowed(currentPosition.moveBy(-1, 0), allowedMovements);
		addIfAllowed(currentPosition.moveBy(0, 1), allowedMovements);
		addIfAllowed(currentPosition.moveBy(0, -1), allowedMovements);
		addIfAllowed(currentPosition.moveBy(1, 1), allowedMovements);
		addIfAllowed(currentPosition.moveBy(1, -1), allowedMovements);
		addIfAllowed(currentPosition.moveBy(-1, -1), allowedMovements);
		addIfAllowed(currentPosition.moveBy(-1, 1), allowedMovements);

		return allowedMovements;
	}

	private void addIfAllowed(Position position, Set<Movement> allowedMovements) {
		if (position.isInsideBoard()) {
			addAndCheckStop(allowedMovements, new Movement(position), board, player);
		}
	}
}
