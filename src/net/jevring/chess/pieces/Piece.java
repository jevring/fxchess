/*
 * Copyright (c) 2013, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.chess.pieces;

import java.util.Set;
import javafx.scene.image.ImageView;
import net.jevring.chess.Movement;
import net.jevring.chess.Player;
import net.jevring.chess.Position;
import net.jevring.chess.board.Board;
import net.jevring.chess.images.Images;

/**
 * A piece is a moveable object on a chess board.
 *
 * @author markus@jevring.net
 */
public abstract class Piece extends ImageView {
	private final ImageView dragImage;
	private final Position startingPosition;
	protected final Player player;

	private Set<Movement> allowedMovements;
	protected Position currentPosition;
	protected Board board;


	protected Piece(String id, Position startingPosition, Player player, Class<? extends Piece> subclass) {
		super(Images.getImageSet().getImage(subclass, player));
		this.startingPosition = startingPosition;
		this.player = player;
		this.currentPosition = startingPosition;
		setId(id);
		setManaged(false); // if it is managed, it will reset to the middle of the frame as soon as the drop is complete

		dragImage = new ImageView(getImage());
		dragImage.setOpacity(0.5);
		dragImage.setMouseTransparent(true);
		dragImage.setManaged(false); // we don't want the parent container to manage the location of the image
	}

	/**
	 * Sets the board that this piece is added to. Knowing the board is required when determining capturing movements
	 * and allowed movements.
	 *
	 * @param board the board that this piece is a member of.
	 */
	public final void addedToBoard(Board board) {
		if (this.board == null) {
			this.board = board;
			this.allowedMovements = calculateAllowedMovements();
		} else {
			throw new IllegalArgumentException("This piece was already added to board " + board);
		}
	}

	public final void pieceMoved(Position to) {
		Set<? extends Position> allowedMovements = getAllowedMovements();
		if (allowedMovements.contains(to) || to.equals(startingPosition)) {
			// allow the starting position during setup
			this.currentPosition = to;
			this.allowedMovements = calculateAllowedMovements();
		} else {
			throw new IllegalArgumentException("Can't move there. Check getAllowedMovements() first!");
		}
	}

	public final ImageView getDragImage() {
		return dragImage;
	}

	/**
	 * While this is called implicitly when the piece is moved, it also has to be called when other 
	 * pieces move, as the board will have changed, and new moves may be possible, while old moves
	 * are no longer valid.
	 */
	public final void updateAllowedMovements() {
		this.allowedMovements = calculateAllowedMovements();
	}

	public final Set<Movement> getAllowedMovements() {
		return allowedMovements;
	}

	public final Position getStartingPosition() {
		return startingPosition;
	}

	public final Player getPlayer() {
		return player;
	}

	public final Position getCurrentPosition() {
		return currentPosition;
	}

	public final Movement getMovementForTarget(Position position) {
		for (Movement allowedMovement : allowedMovements) {
			if (allowedMovement.equals(position)) { // this is fine, a movement is a position
				return allowedMovement;
			}
		}
		return null;
	}


	/**
	 * Calculates the allowed movements. This must include all capturing movements. For instance,
	 * a pawn may only move forward, unless there is another piece diagonally in front of it, in
	 * which case it becomes a capturing move.
	 *
	 * @return a set of allowed movements given the current position and the board
	 */
	public abstract Set<Movement> calculateAllowedMovements();

	protected static boolean addAndCheckStop(Set<Movement> allowedMovements, Movement movement, Board board, Player player) {
		Piece occupant = board.getOccupant(movement);
		if (occupant != null) {
			if (occupant.getPlayer() != player) {
				allowedMovements.add(movement.toCapturing(occupant));
			}
			return true; // we've found something, stop adding moves
		} else {
			allowedMovements.add(movement);
			return false;
		}
	}
}
