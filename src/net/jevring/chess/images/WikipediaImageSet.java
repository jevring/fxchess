/*
 * Copyright (c) 2013, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.chess.images;

import java.util.HashMap;
import java.util.Map;
import javafx.scene.image.Image;
import net.jevring.chess.Player;
import net.jevring.chess.pieces.Bishop;
import net.jevring.chess.pieces.King;
import net.jevring.chess.pieces.Knight;
import net.jevring.chess.pieces.Pawn;
import net.jevring.chess.pieces.Piece;
import net.jevring.chess.pieces.Queen;
import net.jevring.chess.pieces.Rook;

/**
 * Contains images from wikipedia
 *
 * @author markus@jevring.net
 */
public class WikipediaImageSet implements ImageSet {
	private final Map<Class<? extends Piece>, Image> white = new HashMap<>();
	private final Map<Class<? extends Piece>, Image> black = new HashMap<>();

	public WikipediaImageSet() {
		white.put(King.class, new Image(Piece.class.getResourceAsStream("/wikipedia" + Player.White.getDirectory() + "/king.png")));
		white.put(Queen.class, new Image(Piece.class.getResourceAsStream("/wikipedia" + Player.White.getDirectory() + "/queen.png")));
		white.put(Bishop.class, new Image(Piece.class.getResourceAsStream("/wikipedia" + Player.White.getDirectory() + "/bishop.png")));
		white.put(Knight.class, new Image(Piece.class.getResourceAsStream("/wikipedia" + Player.White.getDirectory() + "/knight.png")));
		white.put(Rook.class, new Image(Piece.class.getResourceAsStream("/wikipedia" + Player.White.getDirectory() + "/rook.png")));
		white.put(Pawn.class, new Image(Piece.class.getResourceAsStream("/wikipedia" + Player.White.getDirectory() + "/pawn.png")));

		black.put(King.class, new Image(Piece.class.getResourceAsStream("/wikipedia" + Player.Black.getDirectory() + "/king.png")));
		black.put(Queen.class, new Image(Piece.class.getResourceAsStream("/wikipedia" + Player.Black.getDirectory() + "/queen.png")));
		black.put(Bishop.class, new Image(Piece.class.getResourceAsStream("/wikipedia" + Player.Black.getDirectory() + "/bishop.png")));
		black.put(Knight.class, new Image(Piece.class.getResourceAsStream("/wikipedia" + Player.Black.getDirectory() + "/knight.png")));
		black.put(Rook.class, new Image(Piece.class.getResourceAsStream("/wikipedia" + Player.Black.getDirectory() + "/rook.png")));
		black.put(Pawn.class, new Image(Piece.class.getResourceAsStream("/wikipedia" + Player.Black.getDirectory() + "/pawn.png")));
	}

	@Override
	public Image getImage(Class<? extends Piece> clazz, Player player) {
		if (player == Player.Black) {
			return black.get(clazz);	
		} else {
			return white.get(clazz);
		}
		
	}
}
