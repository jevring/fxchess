/*
 * Copyright (c) 2013, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.chess.options;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.Property;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import net.jevring.chess.board.Square;

/**
 * Provides the various options for the game.
 *
 * @author markus@jevring.net
 */
public class OptionsPane extends VBox {
	public OptionsPane() {
		// the RHS of the expression determines the starting value

		CheckBox highlightValidTargets = new CheckBox("Highlight valid targets");
		highlightValidTargets.selectedProperty().bindBidirectional(Options.INSTANCE.highlightValidTargetsProperty());
		getChildren().add(highlightValidTargets);

		CheckBox highlightHoverOverSquare = new CheckBox("Highlight hover over square");
		highlightHoverOverSquare.selectedProperty().bindBidirectional(Options.INSTANCE.highlightHoveringProperty());
		getChildren().add(highlightHoverOverSquare);

		CheckBox highlightCapturing = new CheckBox("Highlight capturing moves");
		highlightCapturing.selectedProperty().bindBidirectional(Options.INSTANCE.highlightCapturingProperty());
		getChildren().add(highlightCapturing);

		
		// default used to be 4
		Slider strokeThickness = new Slider(1, 20, 10);
		strokeThickness.valueProperty().bindBidirectional(Options.INSTANCE.strokeThicknessProperty());
		getChildren().add(label(strokeThickness, "Stroke thickness"));

		// unfortunately we can't bind this property, so we have to do it "the old-fashioned way"
		final ColorPicker validTargetsColor = new ColorPicker();
		validTargetsColor.valueProperty().bindBidirectional(Options.INSTANCE.validTargetsColorProperty());
		getChildren().add(label(validTargetsColor, "Valid targets highlight color"));

		final ColorPicker hoverColor = new ColorPicker();
		hoverColor.valueProperty().bindBidirectional(Options.INSTANCE.hoveringColorProperty());
		getChildren().add(label(hoverColor, "Hover highlight color"));

		final ColorPicker captureColor = new ColorPicker();
		captureColor.valueProperty().bindBidirectional(Options.INSTANCE.capturingColorProperty());
		getChildren().add(label(captureColor, "Capture highlight color"));
		
		setStyle("-fx-padding: 4; -fx-spacing: 4;");
	}

	private HBox label(Node node, String title) {
		HBox box = new HBox();
		box.getChildren().add(node);
		Label label = new Label(title);
		label.setLabelFor(node);
		box.getChildren().add(label);
		return box;
	}
}
