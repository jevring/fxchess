/*
 * Copyright (c) 2013, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.chess;

import javafx.geometry.Pos;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;

/**
 * Shows dialogs in a handy way.
 *
 * @author markus@jevring.net
 */
public class DialogCreator {
	private final Pane glass = new Pane();
	private final StackPane root;
	private volatile Pane currentDialog;

	public DialogCreator(StackPane root) {
		this.root = root;
	}

	/**
	 * Shows a dialog. Only one dialog may be shown at any one time.
	 * 
	 * @param pane the pane that contains the dialog.
	 * @see #hideDialog()                
	 */
	public void showDialog(Pane pane) {
		// todo: see if we can't use some kind of sliding/folding effect
		hideDialog();
		currentDialog = pane;
		root.getChildren().addAll(glass, pane);
		pane.setVisible(true);
		pane.toFront();
		StackPane.setAlignment(pane, Pos.TOP_CENTER);
		glass.setStyle("-fx-background-color: rgba(112, 64, 16, 0.5)");
		pane.setStyle("-fx-background-color: white");
	}

	/**
	 * Hides whatever the currently showing dialog is.
	 */
	public void hideDialog() {
		if (currentDialog != null) {
			root.getChildren().removeAll(glass, currentDialog);
			currentDialog.setVisible(false);
			currentDialog = null;
		}
	}
}
