/*
 * Copyright (c) 2013, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.chess;

import java.io.IOException;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBoxBuilder;
import javafx.stage.Stage;
import net.jevring.chess.board.Board;
import net.jevring.chess.game.GameInformationPane;
import net.jevring.chess.game.GameInitializer;
import net.jevring.chess.images.Images;
import net.jevring.chess.images.WikipediaImageSet;
import net.jevring.chess.options.OptionsPane;

public class Main extends Application {
	
	public Main() {
		Images.setImageSet(new WikipediaImageSet());
	}

	@Override
    public void start(Stage primaryStage) {
		
		try {
			StackPane stack = new StackPane();
			BorderPane root = new BorderPane();
			OptionsPane optionsPane = new OptionsPane();
			CapturedPieces capturedPieces = new CapturedPieces();
			Board board = new Board(capturedPieces);
			GameInformationPane gameInformationPane = new GameInformationPane();
			DialogCreator dialogCreator = new DialogCreator(stack);
			GameInitializer gameInitializer = new GameInitializer(board, gameInformationPane, dialogCreator);

			root.setLeft(capturedPieces);
			root.setCenter(board); // even doing this, we don't get something that hangs in the middle when we resize the window. Wtf?
			root.setRight(VBoxBuilder.create().children(gameInformationPane, optionsPane).build());
			
			stack.getChildren().add(root);

			// http://rterp.wordpress.com/2013/09/19/drag-and-drop-with-custom-components-in-javafx/

			// binding this does nothing. Maybe the rectangles refuse to scale
			//board.prefHeightProperty().bind(primaryStage.heightProperty());
			//board.prefWidthProperty().bind(primaryStage.widthProperty());

			// Interesting; 
			// we can use parentProperty().addListener() to be notified when we are added to (and presumably removed from) a parent
	    
/*	    
	    root.widthProperty().addListener(new ChangeListener<Number>() {
		    @Override
		    public void changed(ObservableValue<? extends Number> observableValue, Number number, Number number2) {
			    // todo: use something like this to resize.
			    //System.out.println(number2);
		    }
	    });
*/



			// using the pref width/height of the root panel here ensures that the size is correct
			Scene scene = new Scene(stack, stack.getPrefWidth(), stack.getPrefHeight());

			// GAH! Why doesn't sane resizing work?!
			//root.prefWidthProperty().bind(scene.widthProperty());
			//root.prefHeightProperty().bind(scene.heightProperty());

			primaryStage.setTitle("How about a game of chess?");
			primaryStage.setScene(scene);
			primaryStage.show();

			gameInitializer.newGame();
		} catch (IOException e) {
			throw new IllegalStateException("Could not find new game dialog resource!", e);
		}
    }

    public static void main(String[] args) {
        launch(args);
    }
}
