/*
 * Copyright (c) 2013, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.chess.game;

import java.util.ArrayList;
import java.util.List;
import net.jevring.chess.Movement;
import net.jevring.chess.Player;
import net.jevring.chess.board.Board;
import net.jevring.chess.pieces.Piece;

/**
 * Algorithm that chooses random allowed movements.
 *
 * @author markus@jevring.net
 */
public class RandomAllowedMovement implements Algorithm {
	@Override
	public Move nextMove(Board board, Player player) {
		List<Piece> pieces = board.getPieces(player);
		List<Piece> piecesThatCanMove = filterByMovable(pieces);
		// todo: I think we're off-by-one, because with only 2 pieces left, only 1 of them ever moves
		int p = (int) (Math.random() * (piecesThatCanMove.size() - 1));
		Piece piece = piecesThatCanMove.get(p);

		int m = (int) (Math.random() * (piece.getAllowedMovements().size() - 1));
		Movement movement = new ArrayList<>(piece.getAllowedMovements()).get(m);
		
		return new Move(piece, movement);
	}

	
	private List<Piece> filterByMovable(List<Piece> pieces) {
		List<Piece> piecesThatCanMove = new ArrayList<>();
		for (Piece piece : pieces) {
			if (!piece.getAllowedMovements().isEmpty()) {
				piecesThatCanMove.add(piece);
			}
		}
		return piecesThatCanMove;
	}

	@Override
	public String getName() {
		return "Random allowed movement";
	}

	@Override
	public String getDescription() {
		return "Randomly selects a piece and an allowed movement";
	}
}
