/*
 * Copyright (c) 2013, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.chess.game;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import net.jevring.chess.Movement;
import net.jevring.chess.Player;
import net.jevring.chess.board.Board;
import net.jevring.chess.pieces.Piece;

/**
 * An algorithm that chooses a random move, but that will prioritize capturing moves.
 *
 * @author markus@jevring.net
 */
public class CapturingMovementFirst implements Algorithm {
	@Override
	public Move nextMove(Board board, Player player) {
		List<Piece> pieces = board.getPieces(player);
		List<Piece> withAnyMoves = filterWithAnyMoves(pieces);
		List<Piece> withCapturingMoves = filterWithCapturingMoves(withAnyMoves);
		
		Piece piece;
		Movement movement;
		if (!withCapturingMoves.isEmpty()) {
			piece = random(withCapturingMoves);
			movement = random(filterCapturingMoves(piece.getAllowedMovements()));
		} else {
			piece = random(withAnyMoves);
			movement = random(piece.getAllowedMovements());
		}
		
		return new Move(piece, movement);
	}

	private List<Movement> filterCapturingMoves(Set<Movement> allowedMovements) {
		List<Movement> capturingMovements = new ArrayList<>();
		for (Movement allowedMovement : allowedMovements) {
			if (allowedMovement.isCapturing()) {
				capturingMovements.add(allowedMovement);
			}
		}
		
		return capturingMovements;
		
	}


	private <T> T random(Collection<T> items) {
		int index = (int) Math.round(Math.random() * (items.size() - 1));
		if (items instanceof List) {
			return ((List<T>) items).get(index);
		} else {
			return new ArrayList<T>(items).get(index);
		}
	}

	private List<Piece> filterWithAnyMoves(List<Piece> pieces) {
		List<Piece> list = new ArrayList<>();
		for (Piece piece : pieces) {
			if (!piece.getAllowedMovements().isEmpty()) {
				list.add(piece);
			}
		}
		return list;
	}

	private List<Piece> filterWithCapturingMoves(List<Piece> pieces) {
		List<Piece> list = new ArrayList<>();
		for (Piece piece : pieces) {
			for (Movement movement : piece.getAllowedMovements()) {
				if (movement.isCapturing()) {
					list.add(piece);
					break;
				}
			}
		}
		return list;
	}


	@Override
	public String getName() {
		return "Capturing movements first";
	}

	@Override
	public String getDescription() {
		return "Will randomly select pieces and movements, but will prioritize capturing movements";
	}
}
