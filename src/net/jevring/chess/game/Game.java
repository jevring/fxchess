/*
 * Copyright (c) 2013, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.chess.game;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javafx.application.Platform;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import net.jevring.chess.DialogCreator;
import net.jevring.chess.Player;
import net.jevring.chess.board.Board;
import net.jevring.chess.pieces.King;

/**
 * Responsible for keeping track of whose turn it is, and invoking computer player's code.
 *
 * @author markus@jevring.net
 */
public class Game  {
	
	// todo: highlight something when we are in "check"

	// todo: allow exporting and replaying of games (replay at controlled speed, or stepped via button)

	private final ExecutorService executor = Executors.newSingleThreadExecutor();
	private final ObservableList<Move> moves = FXCollections.observableArrayList();
	private final Property<String> currentPlayerDescription = new SimpleStringProperty();
	private final Board board;
	private final Algorithm whiteAlgorithm;
	private final Algorithm blackAlgorithm;
	private final GameInitializer gameInitializer;
	private final DialogCreator dialogCreator;
	
	private Player currentPlayer = Player.White; // white always starts
	private boolean gameOver = false;

	public Game(Board board,
	            Algorithm whiteAlgorithm,
	            Algorithm blackAlgorithm,
	            GameInitializer gameInitializer,
	            DialogCreator dialogCreator) {
		this.board = board;
		this.whiteAlgorithm = whiteAlgorithm;
		this.blackAlgorithm = blackAlgorithm;
		this.gameInitializer = gameInitializer;
		this.dialogCreator = dialogCreator;
		this.currentPlayerDescription.setValue(Player.White.name());
	}

	public boolean isPlayersTurn(Player player) {
		// if it is game over, it is nobody's turn, ever, until the game is reset.
		// only return true here if the player is human. 
		// if the player isn't human, the piece shouldn't be draggable on the board
		return currentPlayer == player && !gameOver && getAlgorithm(currentPlayer) == Human.INSTANCE;
	}
	
	public void pieceMoved(final Move move) {
		if (currentPlayer != move.getPlayer()) {
			throw new IllegalStateException("A player of the wrong color moved. Expected " + currentPlayer + " to move, but it was " + move.getPlayer() + " who did!");
		}
		moves.add(move);
		if (move.getPlayer() == Player.Black) {
			currentPlayer = Player.White;
		} else {
			currentPlayer = Player.Black;
		}
		currentPlayerDescription.setValue(currentPlayer.name());
		if (move.getMovement().isCapturing() && move.getMovement().getCapturedPiece().getClass().equals(King.class)) {
			gameOver(move);
		} else {
			nextComputerMove();	
		}
	}

	private void gameOver(final Move move) {
		gameOver = true;
		final Player winner;
		if (move.getMovement().getCapturedPiece().getPlayer() == Player.White) {
			winner = Player.Black;
		} else {
			winner = Player.White;
		}


		GameOverDialog gameOverDialog = new GameOverDialog(winner, gameInitializer, dialogCreator);


		//pane.setMaxSize(300d, 50d);
		dialogCreator.showDialog(gameOverDialog);
	}

	private void nextComputerMove() {
		if (!gameOver) {
			final Algorithm currentPlayerAlgorithm = getAlgorithm(currentPlayer);
			if (currentPlayerAlgorithm != Human.INSTANCE) {
				// todo: freeze the board, call the algorithm, unfreeze the board
				
				// todo: animate the move so that we see what happens
				executor.execute(new Runnable() {
					@Override
					public void run() {
						// todo: if we wanted something like a progress bar, we'd start a new thread, and update something that was bound to another property
						// http://fxexperience.com/2011/07/worker-threading-in-javafx-2-0/
						try {
							// simulate work
							Thread.sleep(250);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						final Move computerMove = currentPlayerAlgorithm.nextMove(board, currentPlayer);
						if (computerMove.getPlayer() != currentPlayer) {
							throw new IllegalArgumentException("Algorithm generated move for the wrong player! Expected " + currentPlayer + " to move, but it was " + computerMove.getPlayer() + " who did!");
						}
						Platform.runLater(new Runnable() {
							@Override
							public void run() {
								board.move(computerMove);
							}
						});
					}
				});
			}
		}
	}

	private Algorithm getAlgorithm(Player player) {
		if (player == Player.Black) {
			return blackAlgorithm;
		} else {
			return whiteAlgorithm;
		}
	}

	ObservableList<Move> getMoves() {
		return FXCollections.unmodifiableObservableList(moves);
	}

	public ObservableValue<String> currentPlayerProperty() {
		return currentPlayerDescription;
	}

	public String getWhitePlayerDescription() {
		return whiteAlgorithm.getName();
	}

	public String getBlackPlayerDescription() {
		return blackAlgorithm.getName();
	}

	public void start() {
		if (whiteIsHuman()) {
			// todo: perhaps handle this a bit better...
			throw new IllegalArgumentException("White is human, move the piece yourself.");
		} else {
			nextComputerMove();
		}
	}

	public boolean whiteIsHuman() {
		return whiteAlgorithm == Human.INSTANCE;
	}
}
