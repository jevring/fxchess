/*
 * Copyright (c) 2013, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.chess.game;

import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import net.jevring.chess.DialogCreator;
import net.jevring.chess.board.Board;

/**
 * Used to initialize new games.
 *
 * @author markus@jevring.net
 */
public class GameInitializer {
	private final AnchorPane newGameDialog;
	private final Board board;
	private final GameInformationPane gameInformationPane;
	private final DialogCreator dialogCreator;

	public GameInitializer(Board board, GameInformationPane gameInformationPane, DialogCreator dialogCreator) throws IOException {
		this.board = board;
		this.gameInformationPane = gameInformationPane;
		this.dialogCreator = dialogCreator;
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/net/jevring/chess/game/NewGameDialog.fxml"));
		newGameDialog = (AnchorPane) loader.load();
		NewGameDialogController controller = loader.getController();
		controller.setGameInitializer(this);
		gameInformationPane.setGameInitializer(this);
	}

	public void newGame() {
		dialogCreator.showDialog(newGameDialog);
	}
	
	public void createNewGame(Algorithm white, Algorithm black) {
		cancelDialog();
		Game game = new Game(board, white, black, this, dialogCreator);
		gameInformationPane.startNewGame(game);
		board.newGame(game);
	}

	public void cancelDialog() {
		dialogCreator.hideDialog();
	}
}
