/*
 * Copyright (c) 2013, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.chess;

/**
 * Represents a position of a piece. It can also be used to determine legal movements TO a position.
 *
 * @author markus@jevring.net
 */
public class Position {
	protected final int row;
	protected final int column;

	public Position(int row, int column) {
		this.row = row;
		this.column = column;
	}

	public int getRow() {
		return row;
	}

	public int getColumn() {
		return column;
	}

	public Position moveByRows(int rows) {
		return new Position(row + rows, column);
	}

	public Position moveBy(int rows, int columns) {
		return new Position(row + rows, column + columns);
	}

	public Position moveToColumn(int column) {
		return new Position(row, column);
	}

	public Position moveToRow(int row) {
		return new Position(row, column);
	}
	
	public final boolean isInsideBoard() {
		return row <= 7 && row >= 0 && column <= 7 && column >= 0;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Position)) { // this allows us to compare Movement and Position
			return false;
		}

		Position position = (Position) o;

		if (column != position.column) {
			return false;
		}
		if (row != position.row) {
			return false;
		}

		return true;
	}

	@Override
	public int hashCode() {
		int result = row;
		result = 31 * result + column;
		return result;
	}

	@Override
	public String toString() {
		return "Position{" +
				"row=" + row +
				", column=" + column +
				'}';
	}
}
