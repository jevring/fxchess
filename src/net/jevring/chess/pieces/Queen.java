/*
 * Copyright (c) 2013, Markus Jevring <markus@jevring.net>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package net.jevring.chess.pieces;

import java.util.HashSet;
import java.util.Set;
import net.jevring.chess.Movement;
import net.jevring.chess.Player;
import net.jevring.chess.Position;

/**
 * The queen is the most powerful piece. It can move the same way that a {@link net.jevring.chess.pieces.Rook}
 * and a {@link net.jevring.chess.pieces.Bishop} can; diagonally and along a line.
 *
 * @author markus@jevring.net
 */
public class Queen extends Piece {
	public Queen(Player player) {
		super(Queen.class.getSimpleName() + "-" + player, createStartingPosition(player), player, Queen.class);
	}

	private static Position createStartingPosition(Player side) {
		int row = side == Player.Black ? 0 : 7;
		return new Position(row, 3);
	}

	@Override
	public Set<Movement> calculateAllowedMovements() {
		Set<Movement> allowedMovements = new HashSet<>();

		Rook.movementsFromPosition(allowedMovements, currentPosition, board, player);
		Bishop.movementsFromPosition(allowedMovements, currentPosition, board, player);

		return allowedMovements;
	}
}
